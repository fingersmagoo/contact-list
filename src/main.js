// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vueResource from 'vue-resource'
import vueRouter from 'vue-router'
import Hello from './components/Hello'
import Side from './components/Side'
import Contacts from './components/Contacts'

var $ = require('jquery')
Vue.use(vueResource)
Vue.use(vueRouter)
Vue.config.productionTip = false

const routes = [
  { path: '/', component: Hello },
  { path: '/contacts', component: Contacts }
];

const router = new vueRouter({
  routes: routes,
  mode: 'history'
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  template: '<App/>',
  components: { App }
})
